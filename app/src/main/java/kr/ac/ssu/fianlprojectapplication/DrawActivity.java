package kr.ac.ssu.fianlprojectapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class DrawActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout    mLayoutDraw;
    private Button          mBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // initializing ui components
        mLayoutDraw = (LinearLayout) findViewById(R.id.layout_draw);
        mBtnBack = (Button) findViewById(R.id.btn_back);
        mBtnBack.setOnClickListener(this);

        // check extra value
        Intent intent = getIntent();
        if (intent.hasExtra(MainActivity.EXTRAS_DRAW_CHECKED_VALUE)) {
            int value = intent.getIntExtra(MainActivity.EXTRAS_DRAW_CHECKED_VALUE, 0);
            CanvasView view = new CanvasView(this, value);
            mLayoutDraw.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch(viewId) {
            case R.id.btn_back:
                finish();
                break;
        }
    }

    private class CanvasView extends View {

        private int mMode;
        private Paint mPaint;

        public CanvasView(Context context, int mode) {
            super(context);
            mMode = mode;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            // initializing graphic variables
            mPaint = new Paint();
            mPaint.setColor(Color.BLUE);
            mPaint.setAntiAlias(true);
            RectF rectf = new RectF(130, 130, 1030, 1030);


            switch (mMode) {
                case 0:             // draw Arc
                    canvas.drawArc(rectf, 0, 350, true, mPaint);
                    break;
                case 1:             // draw Rect
                    canvas.drawRect(rectf, mPaint);
                    break;
            }
        }
    }
}
