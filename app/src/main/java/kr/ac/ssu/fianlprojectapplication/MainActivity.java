package kr.ac.ssu.fianlprojectapplication;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    public static final String ACTION_BROADCASTING = "kr.ac.ssu.timereciever";
    public static final String EXTRAS_DRAW_CHECKED_VALUE = "draw_value";
    public static final String EXTRAS_TIME_VALUE = "time_value";
    public int mTimeIntervalInSecond = 10;

    private TimeReceiver mTimeReceiver;
    public static boolean bIsThreadRunning;
    private TimeThread mThreadTimeBroadcasting;

    private int mCheckedValue;

    /** UI components **/
    private RadioGroup  mRadioGroup;
    private Button      mBtnDraw;
    private Button      mBtnStartThread;
    private TextView    mTvDisplayTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // register Reciever
        mTimeReceiver = new TimeReceiver();
        registerReceiver(mTimeReceiver, makeIntentFilter());

        // initializing UI component;
        mRadioGroup = (RadioGroup) findViewById(R.id.draw_radio_group);
        mRadioGroup.setOnCheckedChangeListener(this);
        mBtnDraw = (Button) findViewById(R.id.draw_btn);
        mBtnDraw.setOnClickListener(this);
        mBtnStartThread = (Button) findViewById(R.id.thread_btn_start);
        mBtnStartThread.setOnClickListener(this);
        mTvDisplayTime = (TextView) findViewById(R.id.thread_tv_time);

        // initializing draw layout variables
        mRadioGroup.check(R.id.draw_radio_btn_circle);

        // initializing thread variables
        mThreadTimeBroadcasting = new TimeThread();
        bIsThreadRunning = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.draw_btn:
                Intent intent = new Intent(MainActivity.this, DrawActivity.class);
                intent.putExtra(EXTRAS_DRAW_CHECKED_VALUE, mCheckedValue);
                startActivity(intent);
                break;
            case R.id.thread_btn_start:
                if (!bIsThreadRunning) {
                    bIsThreadRunning = true;
                    Toast.makeText(this, "Broadcasting 시작", Toast.LENGTH_SHORT).show();
                    mThreadTimeBroadcasting.start();
                } else {
                    Toast.makeText(this, "Broadcasting이 이미 실행중 입니다", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }



    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.draw_radio_btn_circle:
                mCheckedValue = 0;
                break;
            case R.id.draw_radio_btn_rect:
                mCheckedValue = 1;
                break;
        }
    }


    private class TimeThread extends Thread {
        @Override
        public void run() {
            long intervalInMillis = TimeUnit.SECONDS.toMillis(mTimeIntervalInSecond);
            Intent intent = new Intent(ACTION_BROADCASTING);
            while(bIsThreadRunning) {
                long currentTimeInMillis = System.currentTimeMillis();
                intent.putExtra(EXTRAS_TIME_VALUE, currentTimeInMillis);
                sendBroadcast(intent);
                try {
                    Thread.sleep(intervalInMillis);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bIsThreadRunning = false;
        unregisterReceiver(mTimeReceiver);
    }

    public class TimeReceiver extends BroadcastReceiver{

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_BROADCASTING)) {
                if (intent.hasExtra(EXTRAS_TIME_VALUE)) {
                    long millis = intent.getLongExtra(EXTRAS_TIME_VALUE, System.currentTimeMillis());
                    Date date = new Date(millis);
                    String message = String.format(formatter.format(date)+"에 브로드캐스팅이 발생하였습니다.");
                    mTvDisplayTime.setText(message);
                }
            }
        }
    };

    private static IntentFilter makeIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_BROADCASTING);
        return intentFilter;
    }
}
